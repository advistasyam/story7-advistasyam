$(document).ready(function() {
    $(".accordion_header").click(function(){
        $(".accordion_header").removeClass("active")
        $(this).addClass("active")
    });

    $(".collap").click(function(){
        $('.accordion_header.active').removeClass('active')
    });

    $('.apasi').on('click', function(){
        var parentDiv = $(this).closest('div.accordion_wrap'),
            dir = $(this).data('sort');

        if(dir === 'up'){
            parentDiv.insertBefore( parentDiv.prev() )
        } else if(dir === 'down'){
            parentDiv.insertAfter( parentDiv.next() )
        }
    });

    $('.wrapperoi').sortable();

    $('.apasihh').on('click', function(){
        var ortu = $(this).closest('div.wrapperoi');
        var bodiee = $(document.body);
        var pala = $('.container-fluid');
        ortu.toggleClass("darek")
        bodiee.toggleClass("darek")
        pala.toggleClass("darek")
    });
});