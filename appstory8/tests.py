from selenium import webdriver
from selenium.webdriver.chrome.options import Options

from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from .apps import Appstory8Config
from .views import homepage8

import time

# Create your tests here.

class unitteststory8(TestCase):
    def test_app_name(self):
        self.assertEqual(Appstory8Config.name, "appstory8")
    
    def test_url_homepage_exist8(self):
        response = Client().get('/story8/')
        self.assertEqual(response.status_code, 200)
    
    def test_func_url_homepage8(self):
        found = resolve('/story8/')
        self.assertEqual(found.func, homepage8)
    
    def test_homepage_using_template8(self):
        response = Client().get('/story8/')
        self.assertTemplateUsed(response, "homepage8.html")

class functionalTest(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        
        self.browser = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(functionalTest, self).setUp()

    
    def tearDown(self):
        self.browser.implicitly_wait(3)
        self.browser.quit()
        super(functionalTest, self).tearDown()

    def test_js_down_success(self):
        self.browser.get(self.live_server_url + "/story8") 
        tombol = self.browser.find_element_by_id('button1down')
        tombol.click()

        time.sleep(5) #biar bisa melihat inputan

        hasil = self.browser.find_element_by_css_selector(".accordion_header:nth-child(1)").text

        self.assertIn('js8', self.browser.title)
        self.assertIn("Info 1", hasil)
    
    def test_js_up_success(self):
        self.browser.get(self.live_server_url + "/story8") 
        tombol = self.browser.find_element_by_id('button2up')
        tombol.click()

        time.sleep(5) #biar bisa melihat inputan

        hasil = self.browser.find_element_by_css_selector(".accordion_header:nth-child(1)").text

        self.assertIn('js8', self.browser.title)
        self.assertNotIn("Info 2", hasil)