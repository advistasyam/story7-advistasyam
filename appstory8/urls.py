from django.urls import path
from . import views

app_name = 'appstory8'

urlpatterns = [
    path('', views.homepage8, name='homepage8'),
]