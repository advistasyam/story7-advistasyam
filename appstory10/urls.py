from django.urls import path
from . import views

app_name = "appstory10"

urlpatterns = [
    path('', views.homepage10, name = "homepage10"),
    path('register/', views.register, name='register'),
    path('login/', views.login, name='login'),
    path('logout/', views.logout, name='logout'),
]