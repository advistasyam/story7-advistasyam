from django.shortcuts import render, redirect
from django.contrib.auth.models import User, auth

# Create your views here.
def homepage10(request) :
    return render(request, 'homepage10.html')

def register(request):
    if request.method == 'POST':
        first_name = request.POST['first_name']
        last_name = request.POST['last_name']
        username = request.POST['username']
        email = request.POST['email']
        password1 = request.POST['password1']
        password2 = request.POST['password2']

        if (password1 == password2):
            if User.objects.filter(username=username).exists() :
                passing_message = {
                    'message' : "Username already exist, try using different name",
                }
                return render(request, 'register.html', passing_message)
            else :
                user = User.objects.create_user(username=username, email=email, password=password1, first_name=first_name, last_name=last_name)
                user.save()
        else :
            passing_message = {
                'message' : "Password Mismatch, Please Try Again",
            }
            return render(request, 'register.html', passing_message)

        hasil = "Registration Complete"
        passing_message = {
            'message' : hasil,
        }

        return render(request, 'register.html', passing_message)

    else :
        return render(request, 'register.html')

def login(request):
    if request.method == 'POST':
        username = request.POST['usernamelogin']
        password = request.POST['passwordlogin']

        user = auth.authenticate(username=username, password=password)

        if user is not None:
            auth.login(request, user)
            request.session.set_expiry(300) # auto logout dalam 5 menit
            return redirect ("../")
        
        else :
            passing_message = {
                'message' : "Wrong Username or Password",
            }

            return render(request, 'login.html', passing_message)
    
    else :
        return render(request, 'login.html')

def logout(request):
    auth.logout(request)
    return redirect("../")