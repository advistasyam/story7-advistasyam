from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from .apps import Appstory10Config
from .views import homepage10, register, login, logout
import time

class unitteststory10(TestCase):
    def test_app10_name(self):
        self.assertEqual(Appstory10Config.name, "appstory10")
    
    def test_url_homepage10_exist(self):
        response = Client().get('/story10/')
        self.assertEqual(response.status_code, 200)
    
    def test_url_registration_exist(self):
        response = Client().get('/story10/register')
        self.assertEqual(response.status_code, 301)
    
    def test_url_login_exist(self):
        response = Client().get('/story10/login')
        self.assertEqual(response.status_code, 301)
    
    def test_func_url_homepage10(self):
        found = resolve('/story10/')
        self.assertEqual(found.func, homepage10)
    
    def test_func_url_registration(self):
        found = resolve('/story10/register/')
        self.assertEqual(found.func, register)
    
    def test_func_url_login(self):
        found = resolve('/story10/login/')
        self.assertEqual(found.func, login)
    
    def test_func_url_logout(self):
        found = resolve('/story10/logout/')
        self.assertEqual(found.func, logout)
    
    def test_homepage10_using_template(self):
        response = Client().get('/story10/')
        self.assertTemplateUsed(response, "homepage10.html")
    
    def test_register_using_template(self):
        response = Client().get('/story10/register/')
        self.assertTemplateUsed(response, "register.html")
    
class functionalTest10(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        
        self.browser = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(functionalTest10, self).setUp()

    
    def tearDown(self):
        self.browser.implicitly_wait(3)
        self.browser.quit()
        super(functionalTest10, self).tearDown()

    def test_registration_login_logout_success(self):
        self.browser.get(self.live_server_url + '/story10') 
        tombol_register = self.browser.find_element_by_name('register')
        tombol_register.click()

        time.sleep(5) #biar bisa melihat inputan

        self.assertIn('Registration', self.browser.title)

        username = self.browser.find_element_by_name('username')
        first_name = self.browser.find_element_by_name('first_name')
        last_name = self.browser.find_element_by_name('last_name')
        email = self.browser.find_element_by_name('email')
        password1 = self.browser.find_element_by_name('password1')
        password2 = self.browser.find_element_by_name('password2')

        username.send_keys('tesuser')
        first_name.send_keys('tes')
        last_name.send_keys('user')
        email.send_keys('test@gmail.com')
        password1.send_keys('1234')
        password2.send_keys('123')

        time.sleep(5) #biar bisa melihat inputan

        agree = self.browser.find_element_by_name('regis')
        agree.click()

        self.assertIn("Password Mismatch, Please Try Again", self.browser.page_source)
        time.sleep(5) #biar bisa melihat inputan

        username = self.browser.find_element_by_name('username')
        first_name = self.browser.find_element_by_name('first_name')
        last_name = self.browser.find_element_by_name('last_name')
        email = self.browser.find_element_by_name('email')
        password1 = self.browser.find_element_by_name('password1')
        password2 = self.browser.find_element_by_name('password2')
        
        username.send_keys('tesuser')
        first_name.send_keys('tes')
        last_name.send_keys('user')
        email.send_keys('test@gmail.com')
        password1.send_keys('1234')
        password2.send_keys('1234')

        agree = self.browser.find_element_by_name('regis')
        agree.click()
        self.assertIn("Registration Complete", self.browser.page_source)
        time.sleep(5) #biar bisa melihat inputan

        username = self.browser.find_element_by_name('username')
        first_name = self.browser.find_element_by_name('first_name')
        last_name = self.browser.find_element_by_name('last_name')
        email = self.browser.find_element_by_name('email')
        password1 = self.browser.find_element_by_name('password1')
        password2 = self.browser.find_element_by_name('password2')

        username.send_keys('tesuser')
        first_name.send_keys('tes')
        last_name.send_keys('user')
        email.send_keys('test@gmail.com')
        password1.send_keys('1234')
        password2.send_keys('1234')

        agree = self.browser.find_element_by_name('regis')
        agree.click()
        self.assertIn("Username already exist, try using different name", self.browser.page_source)
        time.sleep(5) #biar bisa melihat inputan

        tombol_login = self.browser.find_element_by_name('kelogin')
        tombol_login.click()
        self.assertIn('Login', self.browser.title)

        username_login = self.browser.find_element_by_name('usernamelogin')
        password_login = self.browser.find_element_by_name('passwordlogin')
        klik_login = self.browser.find_element_by_name('submit')

        username_login.send_keys('tesuser')
        password_login.send_keys('1233')
        klik_login.click()
        self.assertIn("Wrong Username or Password", self.browser.page_source)
        time.sleep(5) #biar bisa melihat inputan

        username_login = self.browser.find_element_by_name('usernamelogin')
        password_login = self.browser.find_element_by_name('passwordlogin')
        klik_login = self.browser.find_element_by_name('submit')

        username_login.send_keys('tesuser')
        password_login.send_keys('1234')
        klik_login.click()
        self.assertIn("Hi there, tesuser", self.browser.page_source)
        time.sleep(5) #biar bisa melihat inputan

        tombol_logout = self.browser.find_element_by_name('kelogout')
        tombol_logout.click()
        self.assertIn("You are not logged in, please login or register", self.browser.page_source)