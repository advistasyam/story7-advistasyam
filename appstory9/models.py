from django.db import models

class Books(models.Model):
    title = models.CharField('Title', max_length=200)
    author = models.CharField('Author', max_length=200)
    publisher = models.CharField('Publisher', max_length=200)
    publishedDate = models.CharField('Published Date', max_length=200)
    likes = models.PositiveIntegerField('Likes')