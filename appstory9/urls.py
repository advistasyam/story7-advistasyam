from django.urls import path
from . import views

app_name = 'appstory9'

urlpatterns = [
    path('', views.homepage9, name='homepage9'),
    path('create_book/', views.create_book, name='create_book'),

]