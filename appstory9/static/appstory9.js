$(document).ready(function() {
    // biar bisa pencet enter dari search
    $('#input_search').on('keyup', function(e) {
        if (e.which === 13 ) {
            searchBook()
            $(this).val('')
        }
    })

    // click tombol search
    $('#button_search').on('click', function() {
        searchBook()
        $('#input_search').val('')
    })

    function searchBook() {
        $.ajax({
            url: 'https://www.googleapis.com/books/v1/volumes',
            type: 'get',
            dataType: 'json',
            data: {
                'q': $('#input_search').val(),
                'maxResults': 20,
            },
        success: function(result) {
            const book = result.items
            $("#book-list").html('')
            $.each(book, function(i, data){
                const info = data.volumeInfo
                let linkBuku = ''
                let authors = ''
                let thumbnail = ''
                $.each(info.authors, function(i, data){
                    if (i === 0) {
                        authors += data
                    } else {
                        authors += ', ' + data
                    }
                })
                thumbnail = info.imageLinks.thumbnail
                linkBuku = info.infoLink
                $("#book-list").append(`
                <div class="card" name="${data.id}">
                    <img src="${thumbnail}"=;" alt="Tidak ada gambar" style="width : 100px" class="gambar">
                    <div class="card-body">
                        <h5 class="card-title"><a href="${linkBuku}" target="_blank">${info.title}</a></h5>
                        <span class="card-publisher">${info.publisher}</span>
                        <span class="card-athor">${authors}</span>
                        <button class="simpan">Simpan buku</button>
                    </div>
                </div>
                `)
                //agar bisa mengirim data ke views.py nya
                // var passing = {
                //     title: info.title,
                //     author: info.authors[0],
                //     publisher: info.publisher,
                //     publishedDate: info.publishedDate,
                // }

                // $('.simpan').on('click', function() {
                //     $.post('http://127.0.0.1:8000/story9/create_book/', passing, function(data){})
                // })
            })
        }
        })
    }

})