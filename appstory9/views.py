from django.forms.models import model_to_dict
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse, HttpResponseForbidden
from .models import Books


# Create your views here.
def homepage9(request):
    return render(request, 'homepage9.html')

@csrf_exempt
def create_book(request) :
    if(request.is_ajax()) :
        Books.objects.create(title=request.POST['title'], author=request.POST['author'], publisher=request.POST['publisher'], publishedDate=request.POST['publishedDate'], likes=0)


