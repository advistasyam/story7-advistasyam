from django.urls import path
from . import views

app_name = 'appstory7'

urlpatterns = [
    path('', views.homepage, name='homepage'),
    path('confirmationpage/', views.confirmation_func, name='confirmation_func'),
]