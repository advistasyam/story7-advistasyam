from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from .apps import Appstory7Config
from .models import dataStatus
from .views import homepage, confirmation_func
from .forms import dataForm
import time

class unitteststory7(TestCase):
    def test_app_name(self):
        self.assertEqual(Appstory7Config.name, "appstory7")
    
    def test_url_homepage_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)
    
    def test_url_confirmation_exist(self):
        response = Client().post('/confirmationpage/', {'nama' : "advis", 'status' : "ini status"})
        self.assertEqual(response.status_code, 200)
    
    def test_func_url_homepage(self):
        found = resolve('/')
        self.assertEqual(found.func, homepage)
    
    def test_func_url_confirmation(self):
        found = resolve('/confirmationpage/')
        self.assertEqual(found.func, confirmation_func)
    
    def test_homepage_using_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, "homepage.html")
    
    def test_confirmation_using_template(self):
        response = Client().post('/confirmationpage/', {'nama' : 'advis', 'status' : 'ini status'})
        self.assertTemplateUsed(response, "confirmation.html")
    
class functionalTest(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        
        self.browser = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(functionalTest, self).setUp()

    
    def tearDown(self):
        self.browser.implicitly_wait(3)
        self.browser.quit()
        super(functionalTest, self).tearDown()

    def test_input_status_success(self):
        self.browser.get(self.live_server_url) 
        name = self.browser.find_element_by_name('nama')
        status = self.browser.find_element_by_name('status')

        name.send_keys('sobat ambyar')
        status.send_keys('pusing test woyyy')

        time.sleep(5) #biar bisa melihat inputan

        submit = self.browser.find_element_by_name('submit')
        submit.click()

        self.assertIn('confirm status', self.browser.title)
        header_text = self.browser.find_element_by_tag_name('h2').text
        self.assertIn('Cek Sebelum Post', header_text)

        agree = self.browser.find_element_by_name('yes')
        agree.click()

        self.assertIn('sobat ambyar', self.browser.page_source)
        self.assertIn('pusing test woyyy', self.browser.page_source)
    
    def test_input_status_batal(self):
        self.browser.get(self.live_server_url)
        name = self.browser.find_element_by_name('nama')
        status = self.browser.find_element_by_name('status')

        name.send_keys('sobat ambyar')
        status.send_keys('pusing test woyyy')

        time.sleep(5) #biar bisa melihat inputan

        submit = self.browser.find_element_by_name('submit')
        submit.click()

        self.assertIn('confirm status', self.browser.title)
        header_text = self.browser.find_element_by_tag_name('h2').text
        self.assertIn('Cek Sebelum Post', header_text)

        abort = self.browser.find_element_by_name('no')
        abort.click()

        self.assertNotIn('sobat ambyar', self.browser.page_source)
        self.assertNotIn('pusing test woyyy', self.browser.page_source)
        
