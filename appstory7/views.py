from django.shortcuts import render, redirect
from .models import dataStatus
from .forms import dataForm

# Create your views here.
def homepage(request):
    if request.method == 'POST':
        nama = request.POST['nama']
        status = request.POST['status']
        dataStatus.objects.create(nama=nama, status=status)
    
    obj = dataStatus.objects.all()
    form = dataForm()
    context = {
		'obj': obj,
		'form' : form
	}
    return render(request, 'homepage.html', context=context)

def confirmation_func(request):
    if request.method == 'POST':
        form = dataForm(request.POST)
        context = {
            'nama' : form.data['nama'],
            'status' : form.data['status']
        }
        return render(request, 'confirmation.html', context=context)
