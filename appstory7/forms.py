from django import forms

from .models import dataStatus

class dataForm(forms.ModelForm):
    class Meta:
        model = dataStatus
        fields = [
            'nama',
            'status',
        ]